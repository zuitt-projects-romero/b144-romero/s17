console.log("Array Manipulation")

// Basic Array Structure
// Access elements in an array - Through index
// Two ways to initialize an array

let array = [1,2,3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr);

// index = array.lengt - 1
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);

let count = ["one", "two", "three", "four"];

//Q	: How are we going to add a new element at the end of an array
// Ans: Using assignment operator (=)
// you cannot add an element on the beginning of an operator

console.log(count.length);
console.log(count[4]);

count[4]="five"
console.log(count);

// Reassignment.
count[0] = "element"
console.log(count)

// Push method array.push()
	// add element at the end of an array
count.push("element")
console.log(count)

function pushMethod(element){
	return count.push(element)
}

pushMethod("six")
pushMethod("seven")
pushMethod("eight")

console.log(count)

// Pop method array.pop()
	// removes the last element of an array
count.pop()

console.log(count)

// Q: Can we use pop method to remove "four"
	// element? - No, it will remove the last element no matter what 

count.pop("four")
console.log(count)

function popMethod(){
	count.pop()
}

popMethod()

console.log(count)

//Q: How do we add element at the beginning of an array
	// Unshift method  array.unshift()
		// adds element at the beginning of an array
	count.unshift("hugot")
	console.log(count)

	function unshiftmethod(name){
		return count.unshift(name)
	}

	unshiftmethod("zero")

	console.log(count)

// Q: can we also remove element at the beginning of an  array? -
	// Shift method array.shift()
		// removes an element at the beginning of an array
	count.shift()
	console.log(count)

	function shiftmethod(){
		count.shift()
	}

	shiftmethod()

	console.log(count)

	let nums = [15, 32, 61, 138, 230, 13, 34];
	nums.sort();
	console.log(nums)

	// sort nums in ascending order

	nums.sort(
		function(a, b){
			// return a - b
			return b - a

		}
	)
	console.log(nums)

	//Reverse method array.reverse()
	nums.reverse()

	console.log(nums)

	// Splice and Slice

	// Splice Method array. splice()
		//returns an array of omitted elements
		//it directly manipulates the original array
		// effects the original array


		// first parameter - index where to start ommiting element
		// second paramter - # of elements to be ommitted starting from the first parameter
		// third parameter - elements to be added in place of the omitted element

	

	/*let newSplice = count.splice(1);
	console.log(newSplice)
	console.log(count)*/

	// let newSplice = count.splice(1, 2);
	// console.log(newSplice)
	// 

/*
	let newSplice = count.splice(1, 2, "a1", "b2", "c3")
	console.log(newSplice)
	console.log(count)*/


	//Slice method array.slice(start, end)

			// first parameter - index where to begin ommiting elements
			// second parameter - # of elements to be ommitted (index - index -1)

	console.log(count)

	// let newSlice = count.slice(1);
	// console.log(newSlice);
	// console.log(count)



	let newSlice = count.slice(1, 5);
	console.log(newSlice);

	// Concat method array.concat()
			// use to merge tw or more arrays
	console.log(count)
	console.log(nums)
	let animals = ["bird", "cat", "dog", "fish"];

	let newConcat = count.concat(nums, animals)
	console.log(newConcat);


	// Join method  array.join() //"", "-", " "
	let meal =["rice", "steak", "jucie"];

	let newJoin = meal.join()
	console.log(newJoin)


	newJoin = meal.join("")
	console.log(newJoin)

	newJoin = meal.join(" ")
	console.log(newJoin)

	newJoin = meal.join("-")
	console.log(newJoin)

	// toString method
	console.log(nums)
	console.log(typeof nums[3])

	let newString = nums.toString()
	console.log(typeof newString)

	/*Accessors*/
	let countries = ["US", "PH", "CAN", "SG", "HK", "PH", "NZ"];
	// indexof() array.index()
		// finds the index of a given element where it is "first" found.
	let index = countries.indexOf("PH")
	console.log(index);

	// if elementis non existing, return is -1
	let example = countries.indexOf("AU")

	// lastIndexof()
	let lastIndex = countries.lastIndexOf("PH")
	console.log(lastIndex)

	if(array.indexOf("AU") == -1){
		 console.log(`Element not existing`)
	}else{
		 console.log(`Element exists in the coutries array`)
	}

	/* Iterators */

		// forEach() array.forEach()
		// map()  array.map()

	let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];


	// forEach
		// returs undefined

		days.forEach(
			function(element){
				console.log(element)
			}



		)

		// map
			//return a copy of an array from the original array which can be manipulated
		let mapDays = days.map(function(day){

			return 	`${day} is the day of the week`
		})

		console.log(mapDays)
		console.log(days)

	/*Mini Activity
		
	*/

	let days2 = [];

	days.forEach(function(days){
		days2.push(days);
	})

	console.log(days2)

	// filter  array.filter(cb())

	console.log(nums)

	let newFilter = nums.filter(function(num){
		// if(num < 50)
		// return (num)

		return num < 50
	})

	console.log(newFilter)

	// includes array.includes()
		// returns boolean value if a word is existing in an array

	console.log(animals)

	let newIncludes = animals.includes("cat")

	console.log(newIncludes)

/*Mini Activity


	create a function that will accept a value, that if values is fount in the array, found in the array, reuturn a message `<value> is found`

	if the value is not found, `return <value> not found!`
	
	Try to invoke function 3x with different values
*/

	function miniActivity(name){
		if (animals.includes(name) == true){

			return `${name} is found`
		}else{
			return `${name} is not found`
		}
	}

	console.log(miniActivity("cat"))
	console.log(miniActivity("bird"))
	console.log(miniActivity("fish"))


	// every(cb())
		// Returns boolean value
		// returns true only if "all  elements" passed the given condition


	let newEvery = nums.every(function(num){
		return(num > 10)
	})
	console.log(newEvery);


	//some(cb())
		//boolean

	let newSome = nums.some(function(num){
		return(num > 50)
	})
	console.log(newEvery);


	// let newSome2 = nums.some(num => num > 50)
	// console.log(newSome2)


	// redue(cb(<previous>, <current>))
	console.log(nums)
	let newReduce = nums.reduce(function(a, b){
		// return a + b
		// return b - a
	})
	console.log(newReduce)

	// to get the average of nums array 
		// total all the elements
		// divide it by total # of elements
	Let average = newReduce / nums.length
// ______________________________________________________

	// toFixed(# of decimals) - returns string

	console.log(average.toFixed(2))

	//parseInt() or parseFloat()
	
	console.log(parseInt(average.toFixed(2)))
	console.log(parseFloat(average.toFixed(2)))